package com.DartChecker;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class einstellungen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        boolean b = true,
                slmode = true;
        if (settings.contains("Theme")) b = settings.getBoolean("Theme", true);
        if (b) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_einstellungen);

        final Switch setmode = findViewById(R.id.slmode);
        final Switch themes = findViewById(R.id.themeswitch);
        TextView umschaltzeit = findViewById(R.id.millisekunden);

        if (settings.contains("changetime"))
            umschaltzeit.setText(Integer.toString(settings.getInt("changetime", 1500)));

        if (settings.contains("setlegmodus")) slmode = settings.getBoolean("setlegmodus", true);
        setmode.setChecked(slmode);
        if (slmode) {
            setmode.setText(getResources().getString(R.string.setlegmodusfirstto));
        } else {
            setmode.setText(getResources().getString(R.string.setlegmodusbestof));
        }

        if (b) {
            themes.setChecked(true);
            themes.setText(getResources().getString(R.string.design_dunkel));
        } else {
            themes.setText(getResources().getString(R.string.design_hell));
            themes.setChecked(false);
        }

        themes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    themes.setText(getResources().getString(R.string.design_dunkel));
                } else {
                    themes.setText(getResources().getString(R.string.design_hell));

                }
                Switch theme = findViewById(R.id.themeswitch);
                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("Theme", theme.isChecked());
                editor.apply();
                //einstellungsactivity neu starten
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        findViewById(R.id.appBarLayout).requestFocus();


        setmode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setmode.setText(getResources().getString(R.string.setlegmodusfirstto));
                } else setmode.setText(getResources().getString(R.string.setlegmodusbestof));

                SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("setlegmodus", setmode.isChecked());
                editor.apply();
            }
        });
    }


    public void onBackPressed() {
        //Einstellungen speichern (bis auf theme, das wurde schon gespeichert)
        TextView millisekunden = findViewById(R.id.millisekunden);
        SharedPreferences settings = getSharedPreferences("Einstellungen", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("changetime", Integer.valueOf(millisekunden.getText().toString()));
        editor.apply();
        finish();
    }
}
