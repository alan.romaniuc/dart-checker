package com.DartChecker;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.Objects;

public class spielende extends AppCompatActivity {

    private final DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    private final DecimalFormat formater = new DecimalFormat("###.##", symbols);

    private final View.OnClickListener okaction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MainActivity.themeauswahl) setTheme(R.style.AppTheme);
        else setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        setContentView(R.layout.activity_spielende);

        TextView e1 = findViewById(R.id.erstert);
        TextView e2 = findViewById(R.id.ersterdaten);
        TextView z1 = findViewById(R.id.zweitert);
        TextView z2 = findViewById(R.id.zweiterdaten);
        TextView d1 = findViewById(R.id.drittert);
        TextView d2 = findViewById(R.id.dritterdaten);
        TextView v1 = findViewById(R.id.viertert);
        TextView v2 = findViewById(R.id.vierterdaten);
        Button okb = findViewById(R.id.okbutton);
        okb.setOnClickListener(okaction);
        TableLayout table = findViewById(R.id.tableLayout);



        Intent intent = getIntent();

        boolean match = intent.getBooleanExtra("match", false);
        if (!match) {


            table.setVisibility(View.GONE);
            int anzahl = intent.getIntExtra("anzahl", 0);

            boolean cricket = intent.getBooleanExtra("cricket", false);

            if (cricket) {
                switch (anzahl) {
                    case 1:
                        e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                        e2.setText("Punkte: " + intent.getIntExtra("ersterscore", 0));
                        z1.setVisibility(View.INVISIBLE);
                        z2.setVisibility(View.INVISIBLE);
                        d1.setVisibility(View.INVISIBLE);
                        d2.setVisibility(View.INVISIBLE);
                        v1.setVisibility(View.INVISIBLE);
                        v2.setVisibility(View.INVISIBLE);
                        break;
                    case 2:
                        e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                        e2.setText("Punkte: " + intent.getIntExtra("ersterscore", 0));
                        z1.setText(intent.getCharSequenceExtra("zweiter"));
                        z2.setText("Punkte: " + intent.getIntExtra("zweiterscore", 0));
                        d1.setVisibility(View.INVISIBLE);
                        d2.setVisibility(View.INVISIBLE);
                        v1.setVisibility(View.INVISIBLE);
                        v2.setVisibility(View.INVISIBLE);
                        break;
                    case 3:
                        e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                        e2.setText("Punkte: " + intent.getIntExtra("ersterscore", 0));
                        z1.setText(intent.getCharSequenceExtra("zweiter"));
                        z2.setText("Punkte: " + intent.getIntExtra("zweiterscore", 0));
                        d1.setText(intent.getCharSequenceExtra("dritter"));
                        d2.setText("Punkte: " + intent.getIntExtra("dritterscore", 0));
                        v1.setVisibility(View.INVISIBLE);
                        v2.setVisibility(View.INVISIBLE);
                        break;
                    case 4:
                        e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                        e2.setText("Punkte: " + intent.getIntExtra("ersterscore", 0));
                        z1.setText(intent.getCharSequenceExtra("zweiter"));
                        z2.setText("Punkte: " + intent.getIntExtra("zweiterscore", 0));
                        d1.setText(intent.getCharSequenceExtra("dritter"));
                        d2.setText("Punkte: " + intent.getIntExtra("dritterscore", 0));
                        v1.setText(intent.getCharSequenceExtra("vierter"));
                        v2.setText("Punkte: " + intent.getIntExtra("vierterscore", 0));
                        break;
                }

                return;
            }


            String darttext = "  " + getResources().getString(R.string.pfeiledd) + " ";
            String resttext = "  " + getResources().getString(R.string.restpunkte) + " ";

            switch (anzahl) {
                case 1:
                    e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                    e2.setText("Ø: " + intent.getCharSequenceExtra("ersterschnitt") + darttext + intent.getCharSequenceExtra("ersterpfeile"));
                    z1.setVisibility(View.INVISIBLE);
                    z2.setVisibility(View.INVISIBLE);
                    d1.setVisibility(View.INVISIBLE);
                    d2.setVisibility(View.INVISIBLE);
                    v1.setVisibility(View.INVISIBLE);
                    v2.setVisibility(View.INVISIBLE);
                    break;
                case 2:
                    e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                    e2.setText("Ø: " + intent.getCharSequenceExtra("ersterschnitt") + darttext + intent.getCharSequenceExtra("ersterpfeile"));
                    z1.setText("2. " + intent.getCharSequenceExtra("zweiter"));
                    z2.setText("Ø: " + intent.getFloatExtra("zweiterschnitt", 0) + darttext + intent.getIntExtra("zweiterpfeile", 0) + resttext + intent.getIntExtra("zweiterrest", 0));
                    d1.setVisibility(View.INVISIBLE);
                    d2.setVisibility(View.INVISIBLE);
                    v1.setVisibility(View.INVISIBLE);
                    v2.setVisibility(View.INVISIBLE);
                    break;
                case 3:
                    e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                    e2.setText("Ø: " + intent.getCharSequenceExtra("ersterschnitt") + darttext + intent.getCharSequenceExtra("ersterpfeile"));
                    z1.setText("2. " + intent.getCharSequenceExtra("zweiter"));
                    z2.setText("Ø: " + intent.getFloatExtra("zweiterschnitt", 0) + darttext + intent.getIntExtra("zweiterpfeile", 0) + resttext + intent.getIntExtra("zweiterrest", 0));
                    d1.setText("3. " + intent.getCharSequenceExtra("dritter"));
                    d2.setText("Ø: " + intent.getFloatExtra("dritterschnitt", 0) + darttext + intent.getIntExtra("dritterpfeile", 0) + resttext + intent.getIntExtra("dritterrest", 0));
                    v1.setVisibility(View.INVISIBLE);
                    v2.setVisibility(View.INVISIBLE);
                    break;
                case 4:
                    e1.setText("1. " + intent.getCharSequenceExtra("erster"));
                    e2.setText("Ø: " + intent.getCharSequenceExtra("ersterschnitt") + darttext + intent.getCharSequenceExtra("ersterpfeile"));
                    z1.setText("2. " + intent.getCharSequenceExtra("zweiter"));
                    z2.setText("Ø: " + intent.getFloatExtra("zweiterschnitt", 0) + darttext + intent.getIntExtra("zweiterpfeile", 0) + resttext + intent.getIntExtra("zweiterrest", 0));
                    d1.setText("3. " + intent.getCharSequenceExtra("dritter"));
                    d2.setText("Ø: " + intent.getFloatExtra("dritterschnitt", 0) + darttext + intent.getIntExtra("dritterpfeile", 0) + resttext + intent.getIntExtra("dritterrest", 0));
                    v1.setText("4. " + intent.getCharSequenceExtra("vierter"));
                    v2.setText("Ø: " + intent.getFloatExtra("vierterschnitt", 0) + darttext + intent.getIntExtra("vierterpfeile", 0) + resttext + intent.getIntExtra("vierterrest", 0));
                    break;

            }

        }
        // matchmode -> kleine matchstatistik ausgeben
        else {
            TextView spielera = findViewById(R.id.spielera);
            TextView spielerb = findViewById(R.id.spielerb);
            TextView sets1 = findViewById(R.id.sets1);
            TextView sets2 = findViewById(R.id.sets2);
            TextView pfeile1 = findViewById(R.id.pfeile1);
            TextView pfeile2 = findViewById(R.id.pfeile2);
            TextView schnitt1 = findViewById(R.id.durchschnitt1);
            TextView schnitt2 = findViewById(R.id.durchschnitt2);
            TextView u61 = findViewById(R.id.u601);
            TextView u62 = findViewById(R.id.u602);
            TextView u1001 = findViewById(R.id.u1001);
            TextView u1002 = findViewById(R.id.u1002);
            TextView u1401 = findViewById(R.id.u1401);
            TextView u1402 = findViewById(R.id.u1402);
            TextView u1801 = findViewById(R.id.u1801);
            TextView u1802 = findViewById(R.id.u1802);
            TextView bauf1 = findViewById(R.id.besterwurf1);
            TextView bauf2 = findViewById(R.id.besterwurf2);
            TextView check1 = findViewById(R.id.checkout1);
            TextView check2 = findViewById(R.id.checkout2);
            TextView legs1 = findViewById(R.id.legs1);
            TextView legs2 = findViewById(R.id.legs2);
            int colorbesser = spielera.getCurrentTextColor(),
                    colorschlechter = spielerb.getCurrentTextColor();
            e1.setVisibility(View.GONE);
            z1.setVisibility(View.GONE);
            d1.setVisibility(View.GONE);
            v1.setVisibility(View.GONE);
            e2.setVisibility(View.GONE);
            z2.setVisibility(View.GONE);
            d2.setVisibility(View.GONE);
            v2.setVisibility(View.GONE);
            table.setVisibility(View.VISIBLE);

            spielera.setText(intent.getCharSequenceExtra("erster"));
            spielerb.setText(intent.getCharSequenceExtra("zweiter"));
            schnitt1.setText(formater.format(intent.getFloatExtra("ersterschnitt", 0)));
            schnitt2.setText(formater.format(intent.getFloatExtra("zweiterschnitt", 0)));
            pfeile1.setText(Integer.toString(intent.getIntExtra("ersterpfeile", 0)));
            pfeile2.setText(Integer.toString(intent.getIntExtra("zweiterpfeile", 0)));
            sets1.setText(Integer.toString(intent.getIntExtra("erstersets", 0)));
            sets2.setText(Integer.toString(intent.getIntExtra("zweitersets", 0)));
            legs1.setText(Integer.toString(intent.getIntExtra("ersterlegs", 0)));
            legs2.setText(Integer.toString(intent.getIntExtra("zweiterlegs", 0)));
            u61.setText(Integer.toString(intent.getIntExtra("erster60", 0)));
            u62.setText(Integer.toString(intent.getIntExtra("zweiter60", 0)));
            u1001.setText(Integer.toString(intent.getIntExtra("erster100", 0)));
            u1002.setText(Integer.toString(intent.getIntExtra("zweiter100", 0)));
            u1401.setText(Integer.toString(intent.getIntExtra("erster140", 0)));
            u1402.setText(Integer.toString(intent.getIntExtra("zweiter140", 0)));
            u1801.setText(Integer.toString(intent.getIntExtra("erster180", 0)));
            u1802.setText(Integer.toString(intent.getIntExtra("zweiter180", 0)));
            bauf1.setText(Integer.toString(intent.getIntExtra("ersterwurf", 0)));
            bauf2.setText(Integer.toString(intent.getIntExtra("zweiterwurf", 0)));
            check1.setText(Integer.toString(intent.getIntExtra("erstercheckout", 0)));
            check2.setText(Integer.toString(intent.getIntExtra("zweitercheckout", 0)));

            if (Float.valueOf(schnitt1.getText().toString()) < Float.valueOf(schnitt2.getText().toString())) {
                schnitt2.setTextColor(colorbesser);
                schnitt1.setTextColor(colorschlechter);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (Objects.equals(Float.valueOf(schnitt1.getText().toString()), Float.valueOf(schnitt2.getText().toString())))
                    schnitt2.setTextColor(colorbesser);
            } else if (Float.valueOf(schnitt1.getText().toString()).equals(Float.valueOf(schnitt2.getText().toString())))
                schnitt2.setTextColor(colorbesser);
            if (Integer.parseInt(pfeile1.getText().toString()) > Integer.parseInt(pfeile2.getText().toString())) {
                pfeile2.setTextColor(colorbesser);
                pfeile1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(pfeile1.getText().toString()) == Integer.parseInt(pfeile2.getText().toString()))
                pfeile2.setTextColor(colorbesser);
            if (Integer.parseInt(legs1.getText().toString()) < Integer.parseInt(legs2.getText().toString())) {
                legs2.setTextColor(colorbesser);
                legs1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(legs1.getText().toString()) == Integer.parseInt(legs2.getText().toString()))
                legs2.setTextColor(colorbesser);
            if (Integer.parseInt(u61.getText().toString()) < Integer.parseInt(u62.getText().toString())) {
                u62.setTextColor(colorbesser);
                u61.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u61.getText().toString()) == Integer.parseInt(u62.getText().toString()))
                u62.setTextColor(colorbesser);
            if (Integer.parseInt(u1001.getText().toString()) < Integer.parseInt(u1002.getText().toString())) {
                u1002.setTextColor(colorbesser);
                u1001.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u1001.getText().toString()) == Integer.parseInt(u1002.getText().toString()))
                u1002.setTextColor(colorbesser);
            if (Integer.parseInt(u1401.getText().toString()) < Integer.parseInt(u1402.getText().toString())) {
                u1402.setTextColor(colorbesser);
                u1401.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u1401.getText().toString()) == Integer.parseInt(u1402.getText().toString()))
                u1402.setTextColor(colorbesser);
            if (Integer.parseInt(u1801.getText().toString()) < Integer.parseInt(u1802.getText().toString())) {
                u1802.setTextColor(colorbesser);
                u1801.setTextColor(colorschlechter);
            } else if (Integer.parseInt(u1801.getText().toString()) == Integer.parseInt(u1802.getText().toString()))
                u1802.setTextColor(colorbesser);
            if (Integer.parseInt(check1.getText().toString()) < Integer.parseInt(check2.getText().toString())) {
                check2.setTextColor(colorbesser);
                check1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(check1.getText().toString()) == Integer.parseInt(check2.getText().toString()))
                check2.setTextColor(colorbesser);
            if (Integer.parseInt(bauf1.getText().toString()) < Integer.parseInt(bauf2.getText().toString())) {
                bauf2.setTextColor(colorbesser);
                bauf1.setTextColor(colorschlechter);
            } else if (Integer.parseInt(bauf1.getText().toString()) == Integer.parseInt(bauf2.getText().toString()))
                bauf2.setTextColor(colorbesser);

        }

    }
}
